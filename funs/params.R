
tsim          <- 100 #Días en tiempo total de la simulación
t.step        <- 0.5 #Step de la simulación no tiene sentido aumentarlo

#Casos iniciales
age_cats <- 4
state    <- c(   S  = rep(999.977/1000, age_cats), 
                 E  = rep(0, age_cats), 
                 I1 = rep(0.023/1000, age_cats), 
                 I2 = rep(0, age_cats), 
                 I3 = rep(0, age_cats),
                 A  = rep(0, age_cats), 
                 M  = rep(0, age_cats), 
                 Q  = rep(0, age_cats), 
                 Q1 = rep(0, age_cats), 
                 Q2 = rep(0, age_cats))


#Parámetros arbitrarios
parameters <- list(
  age_cats                = age_cats,
  saturationI2            = 0.5,
  saturationI3            = 0.2,
  beta.1to2               = rep(1/5, age_cats),  
  beta.AtoR               = rep(1/8, age_cats),
  beta.1toR               = rep(1/8, age_cats),
  beta.3toR               = rep(1/9, age_cats),
  beta.3toM               = rep(1/5, age_cats),
  gamma.1                 = rep(1.3/5, age_cats), 
  gamma.2                 = rep(0, age_cats),
  gamma.3                 = rep(0, age_cats), 
  gamma.A                 = rep(1.3/5, age_cats),
  gamma.E                 = rep(1/5, age_cats),
  p.I3                    = rep(0.16, age_cats),
  p.A                     = rep(0.368, age_cats),
  p.M.saturated           = rep(0.366, age_cats),
  p.M.not.saturated       = rep(0.366, age_cats),
  p.I2.saturated          = rep(0.15, age_cats),
  p.I2.not.saturated      = rep(0.15, age_cats),
  beta.2to3.saturated     = rep(0.15, age_cats),
  beta.2to3.not.saturated = rep(0.15, age_cats),
  beta.2toR.saturated     = rep(0.15, age_cats),
  beta.2toR.not.saturated = rep(0.15, age_cats)
)

demand.parameters <- c(
  camas.censables    = 100,
  camas.no.censables = 100,
  camas.uci          = 100,
  ventiladores       = 100,
  p.V                = 0.71
)