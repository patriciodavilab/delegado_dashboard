library(tidyverse)
library(lubridate)
library(stringr)
library(tidyverse)
library(rdrop2)
library(ggrepel)

#Reading Dropbox
setwd("C:/Users/PatricioDB/Documents/Work/IMSS/delegado_dashboard/")
Noticias <- list.files("data/Final/",pattern="*.md")
Modelo <- list.files()
source("funs/Flexfuns.R")


token <- readRDS("data/Final/token.rds")
rdrop2::drop_acc(dtoken = token)
Nombres <- rdrop2::drop_read_csv("/DatosDelegados/Demograficos.csv",encoding = "UTF-8")
names(Nombres) <- c("Estados",	"POCU",	"Recursos",	"Incapacidades",	"NombreOficial",	"Num")

SearchKey <- "Modelo"
FileSearcher <- function(SearchKey){
  token <- readRDS("data/Final/token.rds")
  rdrop2::drop_acc(dtoken = token)
  Fechas <- rdrop2::drop_dir("/DatosDelegados/") %>% 
    filter(!grepl("*.csv",name))
  FechasOrd <- Fechas %>% select(name) %>% mutate(name2 = dmy(name)) %>% arrange(desc(name2)) %>% mutate(name=as.character(name)) %>%
    mutate(RodriDate = as.character(name2)) %>% 
    mutate(CarlosDate = paste("_",str_replace_all(name,"-",""))) 
  Llegaste <- FALSE
  IndexS <- 1
  while (!Llegaste){
    Archivos <- rdrop2::drop_dir(paste0("/DatosDelegados/",FechasOrd$name[IndexS]))
    if(dim(Archivos)[1]==0){
      NULL
    } else{
      FileSearch <- Archivos %>% 
        filter(grepl(paste0(SearchKey,"*"),name))
      if(dim(FileSearch)[1]==0){
        NULL
      }else{
        File <- rdrop2::drop_read_csv(FileSearch$path_lower,encoding = "UTF-8")
        FechaCorte <- FechasOrd$name2[IndexS]
        Llegaste <- TRUE
      }
    }
    IndexS <- IndexS +1
  }
  return(list(File,FechaCorte))
}


MataAcentos <- function(x){
  # MataAcebtos
  #  In: x un string
  # Out: x un string sin acentos
  #
  # Internamente utiliza un vector de accentos a cambiar y un vector de remplazos.
  accents <- c("Á","á","É","é","Í","í","Ó","ó","Ú","ú")
  rep     <- c("A","A","E","E","I","I","O","O","U","U")
  for(i in 1:length(rep)) {
    x <- str_replace_all(x,accents[i],rep[i])
  }
  return(toupper(x))
  
  
}
Incapacidades <- FileSearcher("data_incapacidades")
IncapacidadesData <- Incapacidades[[1]] %>% as_tibble() %>% 
  left_join(Nombres, by = c("Estado" = "Incapacidades"))
FechaCorteIncapacidades <- Incapacidades[[2]]

Modelo <- FileSearcher("Modelo")
Estados <- Modelo[[1]] %>% 
filter(Lugar != "TIJUANA") %>% 
  left_join(Nombres, by = c("Lugar" = "Estados"))
FechaCorteModelo <- Modelo[[2]]

##Read Pocu_IMSS
pocu_imssBig <- FileSearcher("CIprop")
pocu_imss <- pocu_imssBig[[1]] %>% as_tibble()
PocuNames <- names(pocu_imss) %>% str_replace_all("X","") %>% str_replace_all("[.]","-")
PocuNames[1] <- "X1"
names(pocu_imss) <- PocuNames
pocu_imss <- pocu_imss %>% 
  gather(key = fecha, value = val, -X1) %>% 
  mutate(fecha = as.Date(fecha), X1=str_replace(str_replace(X1,'inf',''), 'sup',''))
days <- dim(pocu_imss)[1]/(32*3)
pocu_imss$indicador <- rep(c('PropEstimada', 'LimInf', 'LimSup'),length(pocu_imss$X1)/3)
pocu_imss<-pocu_imss %>% spread(key = indicador, value = val) %>% rename(Estado=X1) %>% filter(fecha >= "2020-04-07") %>% 
  left_join(Nombres, by = c("Estado" = "POCU"))
FechaCortePocu <- pocu_imssBig[[2]]
#Camas todos
pocu_totalBig <- FileSearcher("Cprop")
pocu_total <- pocu_imssBig[[1]] %>% as_tibble()
PocuNames <- names(pocu_total) %>% str_replace_all("X","") %>% str_replace_all("[.]","-")
PocuNames[1] <- "X1"
names(pocu_total) <- PocuNames
pocu_total <- pocu_total %>% 
  gather(key = fecha, value = val, -X1) %>% 
  mutate(fecha = as.Date(fecha), X1=str_replace(str_replace(X1,'inf',''), 'sup',''))
days <- dim(pocu_total)[1]/(32*3)
pocu_total$indicador <- rep(c('PropEstimada', 'LimInf', 'LimSup'),length(pocu_total$X1)/3)
pocu_total<-pocu_total %>% spread(key = indicador, value = val) %>% rename(Estado=X1) %>% filter(fecha >= "2020-04-07") %>% 
  left_join(Nombres, by = c("Estado" = "POCU"))
  
FechaCortePocu2 <- pocu_totalBig[[2]]


##Evolucion de Camas IMSS
ggplot(data = pocu_imss) +
  geom_line(aes(x = fecha, y = PropEstimada)) +
  geom_rect(aes(xmin = min(fecha), xmax = max(fecha), ymin = 0.5, ymax = 0.8),
            fill = "orange", alpha = 0.01) +
  geom_rect(aes(xmin = min(fecha), xmax = max(fecha), ymin = 0.8, ymax = 1),
            fill = "red", alpha = 0.01) +
  geom_rect(aes(xmin = min(fecha), xmax = max(fecha), ymin = 0, ymax = 0.5),
            fill = "deepskyblue3", alpha = 0.01) +
  geom_rect(aes(xmin = min(fecha), xmax = max(fecha), ymin = 1, ymax = 1.5),
            fill = "purple", alpha = 0.01) +
  geom_ribbon(aes(x = fecha, ymin=LimInf, ymax=LimSup), linetype=2, show.legend=FALSE, alpha=0.5) +
  coord_cartesian(ylim = c(0, 1.5)) +
  geom_vline(xintercept = as.Date(FechaCortePocu2, format='%d%m%Y'), color='Black', linetype='dashed') +
  ggtitle(paste("Evolución de la tasa de ocupación hospitalaria"))  + ylab("Proporción estimada de camas ocupadas IMSS") +
  guides(col = guide_legend(ncol = 1)) + theme(axis.text.x = element_text(angle = 60, hjust = 1),
                                               plot.title = element_text(hjust = 0.5))+
  facet_wrap(~Estado, nrow=4)+ scale_y_continuous(labels = scales::percent_format(accuracy = 1))+
  geom_label_repel(data = pocu_imss[pocu_imss$PropEstimada > .8,] %>% group_by(Estado) %>% slice(1) %>% rbind(
    pocu_imss[pocu_imss$PropEstimada > 1,] %>% group_by(Estado) %>% slice(1)
  ), aes(fecha,PropEstimada,label=as.character(fecha)),
                   nudge_y       = 16 - pocu_imss[pocu_imss$PropEstimada > .8,] %>% group_by(Estado) %>% slice(1) %>% rbind(
                     pocu_imss[pocu_imss$PropEstimada > 1,] %>% group_by(Estado) %>% slice(1)
                   )%>% .$PropEstimada,
                   size          = 2,
                   box.padding   = 0.5,
                   point.padding = 0.5,
                   force         = 100,
                   segment.size  = 0.2,
                   segment.color = "grey50",
                   direction     = "x")+
  theme(plot.background =  element_rect(fill = "#0c6f5f", color = NA),
        panel.background =  element_rect(fill = "#0c6f5f", color = NA),
        axis.text = element_text(colour = "white"))+
  theme(plot.title = element_text(size = 17, face = "bold"))

#Evolucion de camas totales
#Cortar en 150% 
ggplot(data = pocu_total) +
  geom_line(aes(x = fecha, y = PropEstimada)) +
  geom_rect(aes(xmin = min(fecha), xmax = max(fecha), ymin = 0.5, ymax = 0.8),
            fill = "orange", alpha = 0.01) +
  geom_rect(aes(xmin = min(fecha), xmax = max(fecha), ymin = 0.8, ymax = 1),
            fill = "red", alpha = 0.01) +
  geom_rect(aes(xmin = min(fecha), xmax = max(fecha), ymin = 0, ymax = 0.5),
            fill = "deepskyblue3", alpha = 0.01) +
  geom_rect(aes(xmin = min(fecha), xmax = max(fecha), ymin = 1, ymax = 1.5),
            fill = "purple", alpha = 0.01) +
  geom_ribbon(aes(x = fecha, ymin=LimInf, ymax=LimSup), linetype=2, show.legend=FALSE, alpha=0.5) +
  coord_cartesian(ylim = c(0, 1.5)) +
  geom_vline(xintercept = as.Date(FechaCortePocu2,format='%d%m%Y'), color='Black', linetype='dashed') +
  ggtitle("Evolución en la ocupación hospitalaria sector publico + IMSS") + xlab("Día") + ylab("Proporción estimada de camas ocupadas") +
  guides(col = guide_legend(ncol = 1)) + theme(axis.text.x = element_text(angle = 60, hjust = 1),
                                               plot.title = element_text(hjust = 0.5))+
  facet_wrap(~Estado, nrow=4)+ scale_y_continuous(labels = scales::percent_format(accuracy = 1))+
  geom_label_repel(data = pocu_imss[pocu_imss$PropEstimada > .8,] %>% group_by(Estado) %>% slice(1) %>% rbind(
    pocu_imss[pocu_imss$PropEstimada > 1,] %>% group_by(Estado) %>% slice(1)
  ), aes(fecha,PropEstimada,label=as.character(fecha)),
                   nudge_y       = 16 - pocu_imss[pocu_imss$PropEstimada > .8,] %>% group_by(Estado) %>% slice(1) %>% rbind(
                     pocu_imss[pocu_imss$PropEstimada > 1,] %>% group_by(Estado) %>% slice(1)) %>% .$PropEstimada,
                   size          = 2,
                   box.padding   = 0.5,
                   point.padding = 0.5,
                   force         = 100,
                   segment.size  = 0.2,
                   segment.color = "grey50",
                   direction     = "x")+
  theme(plot.background =  element_rect(fill = "#dfcaa3", color = NA),
        panel.background =  element_rect(fill = "#dfcaa3", color = NA),
        axis.text = element_text(colour = "black"))+
  theme(plot.title = element_text(size = 13.5, face = "bold"))

#Evolucion Camas Estado IMSS
Estado <- "AGUASCALIENTES"
Lugar <- "BAJA CALIFORNIA NORTE"

pocu_imss %>% 
  filter(NombreOficial == Lugar) %>% 
ggplot(aes(x = fecha, y = PropEstimada)) +
  geom_rect(aes(xmin = min(fecha), xmax = max(fecha), ymin = 0.5, ymax = 0.8),
            fill = "orange", alpha = 0.01) +
  geom_rect(aes(xmin = min(fecha), xmax = max(fecha), ymin = 0.8, ymax = 1),
            fill = "red", alpha = 0.01) +
  geom_rect(aes(xmin = min(fecha), xmax = max(fecha), ymin = 0, ymax = 0.5),
            fill = "deepskyblue3", alpha = 0.01) +
  geom_rect(aes(xmin = min(fecha), xmax = max(fecha), ymin = 1, ymax = 1.5),
            fill = "purple", alpha = 0.01) +
  geom_line(size=1.1)+
  geom_point()+
  geom_ribbon(aes(x = fecha, ymin=LimInf, ymax=LimSup), linetype=1, show.legend=FALSE, alpha=0.5) +
  coord_cartesian(ylim = c(0, 1.5)) +
  geom_vline(xintercept = as.Date(FechaCortePocu2,format='%d%m%Y'), color='Black', linetype='dashed') +
  xlab("SINOLAVE") + ylab("Proporción estimada de camas ocupadas IMSS")+
  scale_y_continuous(n.breaks = 5, labels = scales::percent) +
  #ggtitle(paste0("[",Estado, "] IMSS"))+
  ggtitle("Evolución en la ocupación hospitalaria IMSS")+
  theme_bw()+
  scale_x_date(date_breaks = "1 week") +
  theme(axis.text.x = element_text(angle = 90))+
  geom_label_repel(data = pocu_imss[pocu_imss$PropEstimada > .8,] %>% filter(NombreOficial == Lugar) %>% slice(1) %>% rbind(
    pocu_imss[pocu_imss$PropEstimada > 1,] %>% filter(NombreOficial == Lugar) %>% slice(1)
  ), aes(fecha,PropEstimada,label=as.character(fecha)),
                   nudge_y       = 16 - pocu_imss[pocu_imss$PropEstimada > .8,] %>% filter(NombreOficial == Lugar) %>% slice(1) %>% rbind(
                     pocu_imss[pocu_imss$PropEstimada > 1,] %>% filter(NombreOficial == Lugar) %>% slice(1)
                   ) %>% .$PropEstimada,
                   size          = 4,
                   box.padding   = 0.5,
                   point.padding = 0.5,
                   force         = 100,
                   segment.size  = 0.2,
                   segment.color = "grey50",
                   direction     = "x") +
  theme(plot.background =  element_rect(fill = "#0c6f5f", color = NA),
        panel.background =  element_rect(fill = "#0c6f5f", color = NA),
        axis.text = element_text(colour = "white"))+
  theme(plot.title = element_text(size = 17, face = "bold"))



#Evolucion Camas Estado Totales

pocu_total %>% 
  filter(NombreOficial == Lugar) %>% 
  ggplot(aes(x = fecha, y = PropEstimada)) +
  geom_rect(aes(xmin = min(fecha), xmax = max(fecha), ymin = 0.5, ymax = 0.8),
            fill = "yellow", alpha = 0.005) +
  geom_rect(aes(xmin = min(fecha), xmax = max(fecha), ymin = 0.8, ymax = 1),
            fill = "red", alpha = 0.005) +
  geom_rect(aes(xmin = min(fecha), xmax = max(fecha), ymin = 0, ymax = 0.5),
            fill = "#78c2ad", alpha = 0.05) +
  geom_rect(aes(xmin = min(fecha), xmax = max(fecha), ymin = 1, ymax = 1.5),
            fill = "purple", alpha = 0.005) +
  geom_line()+
  geom_point()+
  geom_ribbon(aes(x = fecha, ymin=LimInf, ymax=LimSup), linetype=2, show.legend=FALSE, alpha=0.5) +
  coord_cartesian(ylim = c(0, 1.5)) +
  geom_vline(xintercept = as.Date(FechaCortePocu2,format='%d%m%Y'), color='Black', linetype='dashed') +
  xlab("SINOLAVE") + ylab("Proporción estimada de camas ocupadas")+
  scale_y_continuous(n.breaks = 5, labels = scales::percent) +
  ggtitle("Evolución en la ocupación hospitalaria sector publico + IMSS")+
  #labs(subtitle = "Evolución en la ocupación hospitalaria", caption = expression(paste("Datos del ", italic(sinolave))))+
  theme_bw()+
  scale_x_date(date_breaks = "1 week") +
  theme(axis.text.x = element_text(angle = 90))+
  geom_label_repel(data = pocu_total[pocu_total$PropEstimada > .8,] %>% filter(NombreOficial == Lugar) %>% slice(1) %>% rbind(
    pocu_total[pocu_total$PropEstimada > 1,] %>% filter(NombreOficial == Lugar) %>% slice(1)
  ), aes(fecha,PropEstimada,label=as.character(fecha)),
  nudge_y       = 16 - pocu_total[pocu_total$PropEstimada > .8,] %>% filter(NombreOficial == Lugar) %>% slice(1) %>% rbind(
    pocu_total[pocu_total$PropEstimada > 1,] %>% filter(NombreOficial == Lugar) %>% slice(1)
  ) %>% .$PropEstimada,
  size          = 4,
  box.padding   = 0.5,
  point.padding = 0.5,
  force         = 100,
  segment.size  = 0.2,
  segment.color = "grey50",
  direction     = "x") +
  theme(plot.background =  element_rect(fill = "#dfcaa3", color = NA),
        panel.background =  element_rect(fill = "#dfcaa3", color = NA),
        axis.text = element_text(colour = "black"))+
  theme(plot.title = element_text(size = 13.5, face = "bold"))

##Grafica Pred_Llegadas,Bajo,Alto vs Obs_Llegadas
Estados %>% 
  filter(NombreOficial == Estado) %>%
  mutate(Obs_Llegadas = ifelse(is.na(Obs_Llegadas)& as.Date(Fecha)<FechaCorteModelo,0,Obs_Llegadas)) %>% 
  mutate(Obs_Llegadas = ifelse(as.Date(Fecha) > FechaCorteModelo,NA,Obs_Llegadas)) %>% 
  ggplot(aes(x=as.Date(Fecha)))+
  geom_line(aes(y = Pred_Llegadas_Bajo), linetype = "dotted") +
  geom_line(aes(y = Pred_Llegadas_Alto), linetype = "dotted") +
  geom_col(aes(y = Pred_Llegadas),color="white") +
  geom_point(aes(y = Obs_Llegadas), color = "red", size = 1.2) +
  #geom_point(aes(y = Obs_Llegadas), color = "black", size = 1) +
  geom_ribbon(aes(ymin = Pred_Llegadas_Bajo, ymax = Pred_Llegadas_Alto), alpha = 0.1, fill = "#04e51b",color = "#04e51b") +
  geom_vline(xintercept = as.Date(FechaCorteModelo,format='%d%m%Y'), color='Black', linetype='dashed') +
  xlab("SINOLAVE") + ylab("Numero de individuos")+
  scale_x_date(date_breaks = "1 week",date_minor_breaks = "1 day") +
  theme_bw()+
  theme(axis.text.x = element_text(angle = 90)) +
  ggtitle("Nuevas hospitalizaciones diarias observadas")+
  theme(plot.title = element_text(size = 17, face = "bold"))


##Grafica Obs_Hospital vs Pred_Bajo_Hospitalizado" "Pred_Hospitalizado"      "Pred_Alto_Hospitalizado"
Estados %>% 
  filter(NombreOficial == Estado) %>%
  ggplot(aes(x=as.Date(Fecha)))+
  geom_line(aes(y = Pred_Bajo_Hospitalizado), linetype = "dotted") +
  #geom_line(aes(y = Pred_Hospitalizado), linetype = "dotted") +
  geom_line(aes(y = Pred_Alto_Hospitalizado), linetype = "dotted") +
  geom_point(aes(y = Obs_Hospital), color = "red", size = 1) +
  #geom_line(aes(y = Obs_Hospital_Incidente), color = "red", size = 1) +
  geom_ribbon(aes(ymin = Pred_Bajo_Hospitalizado, ymax = Pred_Alto_Hospitalizado), alpha = 0.1, fill = "#04e51b",color = "#04e51b") +
  geom_vline(xintercept = as.Date(FechaCorteModelo,format='%d%m%Y'), color='Black', linetype='dashed') +
  xlab("SINOLAVE") + 
  ylab("Número de casos en hospitalización (al mismo tiempo)")+
  scale_x_date(date_breaks = "1 week",date_minor_breaks = "1 day") +
  theme_bw()+
  theme(axis.text.x = element_text(angle = 90)) +
  ggtitle("HOSPITALIZACIONES")+
  theme(plot.title = element_text(size = 17, face = "bold"))



#Obs_UCI vs Pred_Bajo_UCI","Pred_UCI","Pred_Alto_UCI" 
Estados %>% 
  filter(NombreOficial == Estado) %>%
  ggplot(aes(x=as.Date(Fecha)))+
  geom_line(aes(y = Pred_Bajo_UCI), linetype = "dotted") +
  #geom_line(aes(y = Pred_UCI), linetype = "dotted") +
  geom_line(aes(y = Pred_Alto_UCI), linetype = "dotted") +
  geom_point(aes(y = Obs_UCI), color = "red", size = 1) +
  #geom_line(aes(y = Obs_UCI_Incidente), color = "red", size = 1) +
  geom_ribbon(aes(ymin = Pred_Bajo_UCI, ymax = Pred_Alto_UCI), alpha = 0.1, fill = "#04e51b",color = "#04e51b") +
  geom_vline(xintercept = as.Date(FechaCorteModelo,format='%d%m%Y'), color='Black', linetype='dashed') +
  xlab("SINOLAVE") + 
  ylab("Número de casos en hospitalización (al mismo tiempo)")+
  scale_x_date(date_breaks = "1 week",date_minor_breaks = "1 day") +
  theme_bw()+
  theme(axis.text.x = element_text(angle = 90)) +
  ggtitle("UNIDADES DE CUIDADO INTENSIVO")+
  theme(plot.title = element_text(size = 17, face = "bold"))

#Obs_Alta_Mejora_Egreso vs Pred_Bajo_Alta_Mejoria"  "Pred_Alta_Mejoria"       "Pred_Alto_Alta_Mejoria"
#(Alta Mejora Egreso?)
Estados %>% 
  filter(NombreOficial == Estado) %>%
  ggplot(aes(x=as.Date(Fecha)))+
  geom_line(aes(y = Pred_Bajo_Alta_Mejoria), linetype = "dotted") +
  #geom_line(aes(y = Pred_Alta_Mejoria), linetype = "dotted") +
  geom_line(aes(y = Pred_Alto_Alta_Mejoria), linetype = "dotted") +
  #geom_line(aes(y = Obs_Alta_Mejoria_Egreso_Incidente), color = "red", size = 1) +
  geom_line(aes(y = Obs_Alta_Mejoria_Egreso), color = "red", size = 1) +
  #geom_line(aes(y = Alta_Mejoria_Egreso), color = "black", size = 1) +
  geom_ribbon(aes(ymin = Pred_Bajo_Alta_Mejoria, ymax = Pred_Alto_Alta_Mejoria), alpha = 0.1, fill = "#04e51b",color = "#04e51b") +
  geom_vline(xintercept = as.Date(FechaCorteModelo,format='%d%m%Y'), color='Black', linetype='dashed') +
  xlab("SINOLAVE") + ylab("Casos Acumulados")+
  scale_x_date(date_breaks = "1 week",date_minor_breaks = "1 day") +
  theme_bw()+
  theme(axis.text.x = element_text(angle = 90)) +
  ggtitle("ALTAS POR MEJORÍA O TRASLADO")+
  theme(plot.title = element_text(size = 17, face = "bold"))

#Muerte vs Pred_Bajo_Muerte
Estados %>% 
  filter(NombreOficial == Estado) %>%
  ggplot(aes(x=as.Date(Fecha)))+
  geom_line(aes(y = Pred_Bajo_Muerte), linetype = "dotted") +
  #geom_line(aes(y = Pred_Muerte), linetype = "dotted") +
  geom_line(aes(y = Pred_Alto_Muerte), linetype = "dotted") +
  #geom_line(aes(y = Muerte_Incidente), color = "red", size = 1) +
  geom_point(aes(y = Muerte), color = "red", size = 1) +
  geom_ribbon(aes(ymin = Pred_Bajo_Muerte, ymax = Pred_Alto_Muerte), alpha = 0.1, fill = "#04e51b",color = "#04e51b") +
  geom_vline(xintercept = as.Date(FechaCorteModelo,format='%d%m%Y'), color='Black', linetype='dashed') +
  xlab("SINOLAVE") + ylab("Casos Acumulados")+
  scale_x_date(date_breaks = "1 week",date_minor_breaks = "1 day") +
  theme_bw()+
  theme(axis.text.x = element_text(angle = 90)) +
  ggtitle("ALTAS POR DEFUNCIÓN")+
  theme(plot.title = element_text(size = 17, face = "bold"))




#Tasas
#Hola no sé para las tasas de incidencia qué ocuparemos para la visualizacion... 
#pero te mando el archivo y su catalogo para clave inegi (cve_entidad) 
#  En ese archivo es la columna de Inc.  es incidencia.
Tasas <- FileSearcher("tasas")
TasasData <- Tasas[[1]] %>% as_tibble() %>% 
  left_join(Nombres, by = c("X" = "POCU"))
FechaCorteIncapacidades <- Tasas[[2]]

#Tasas <- read_csv("data/Final/tasas2020.05.11.csv")

Recursos <- FileSearcher("recursos")
RecursosData <- Recursos[[1]]%>% 
  left_join(Nombres, by = c("entidad" = "Recursos"))
RecursosCorte <- Recursos[[2]]

mexico <- st_read(dsn = 'data/Maps',
                  layer = '00ent')

datos_pareja <- merge(TasasData %>% mutate(Num = ifelse(Num < 10, paste0("0",Num),Num)) %>% filter(Num != "00"), mexico, by.x = 'Num', by.y = 'CVEGEO', all.y = T)

p <- ggplot(datos_pareja %>% select(geometry,inc)) +
  geom_sf(aes(fill = inc, geometry = geometry), color = 'black', size = 0.3)+
  #guides(fill = guide_legend(reverse = T)) +
  theme(
    plot.background = element_rect(fill = 'white'),
    panel.background = element_rect(fill = 'white'),
    axis.line = element_blank(),
    axis.title = element_blank(),
    axis.ticks = element_blank(),
    axis.text = element_blank(),
    plot.title = element_text(family = 'Times', size = 24, color = 'black',
                              hjust = 0, face = 'bold'),
    plot.subtitle = element_text(family = 'Times', size = 12, color = 'black',
                                 hjust = 0),
    plot.caption = element_text(family = 'Times', size = 14, color = 'black', hjust = 1),
    legend.direction = 'vertical',
    legend.position = 'right',
    legend.text = element_text(family = 'Times', size = 14, color = 'black'),
    legend.title = element_text(family = 'Times', size = 14, color = 'black'),
    legend.justification="center"
  )

##Noticias.
#VER APP


##Tablas


RecursosTablaGen <- function(Recursos, Estado){
  Nombres <- c("Concepto", "Medida")
  TotalCamas <- c("Total camas")
  TotalCamasUCI <- c("Total camas UCI")
  CamasOcupadas <- c("Camas ocupadas COVID")
  CamasOcupadasUCI <- c("Camas ocupadas UCI COVID")
  PorcOcupacion <- c("Pacientes Hospitalizados")
  MedicosInfectados <- c("# de medicos infectados")
  TasaMedicosInfectados <- c("Tasa de medicos infectados")
  UltimaF <- Recursos %>% filter(NombreOficial == Estado) 

  ##Total Camas
  TotalCamas <- c(TotalCamas,UltimaF %>% select(camas_totales))
  names(TotalCamas) <- Nombres
  TotalCamas <- TotalCamas %>% as_tibble()
  #Total Camas UCI
  TotalCamasUCI <- c(TotalCamasUCI,UltimaF %>% select(camas_uci))
  names(TotalCamasUCI) <- Nombres
  TotalCamasUCI <- TotalCamasUCI %>% as_tibble()
  ##CamasOcupadas
  CamasOcupadas <- c(CamasOcupadas,UltimaF %>% select(camas_hosp_covid_ocupadas))
  names(CamasOcupadas) <- Nombres
  CamasOcupadas <- CamasOcupadas %>% as_tibble()
  ##Camas
  CamasOcupadasUCI <- c(CamasOcupadasUCI,UltimaF %>% select(camas_uci_covid_ocupadas))
  names(CamasOcupadasUCI) <- Nombres
  CamasOcupadasUCI <- CamasOcupadasUCI %>% as_tibble()
  ##PorcOcupacion
  #Visualizacion. Opcion visualizar % o por persona.
  PorcOcupacion <- c(PorcOcupacion,UltimaF %>%  select(pacientes_hosp))
  names(PorcOcupacion) <- Nombres
  PorcOcupacion <- PorcOcupacion %>% as_tibble()
  ##MedicosInfectados. No esta. 
  #MedicosInfectados <- c(MedicosInfectados,Placeholder)
  #names(MedicosInfectados) <- Nombres
  #MedicosInfectados <- MedicosInfectados %>% as_tibble()
  ####TasaMedicosInfectados. No esta.
  #TasaMedicosInfectados <- c(TasaMedicosInfectados,Placeholder)
  #names(TasaMedicosInfectados) <- Nombres
  #TasaMedicosInfectados <- TasaMedicosInfectados %>% as_tibble()
  
  Tabla <- rbind(
    TotalCamas,
    TotalCamasUCI,
    CamasOcupadas,
    CamasOcupadasUCI,
    PorcOcupacion
    #MedicosInfectados,
    #TasaMedicosInfectados
  )
  
}


EpiTablaGen <- function(Tasa,Estados,Estado){
  Nombres <- c("Concepto", "Medida")
  R0Actual<-c("R0")
  TotalConfirmados<-c("Total casos confirmados")
  TasaIncidencia10<-c("Tasa de incidencia x10,000 hab")
  #HOLD off. "Estimadose..."
  #TotalMuertos<-c("Tasa de mortalidad")
  #TasaLetalidad<-c("Tasa de letalidad")
  #Egresos. Altas hospitalarias.
  Recuperados<-c("Recuperados")
  
  UltimaF <- Estados %>% 
    #filter(!is.na(R0)) %>% 
    filter(NombreOficial == Estado) %>% 
    #slice(which.max(ymd(Fecha)))
    filter(as.Date(Fecha ,origin = "1970-01-01") == as.Date(fecha_corte,format='%d%m%Y'))
  
  
  Placeholder <- UltimaF %>% select(Camas)
  ##Rolling Mean Code
  #mutate(Promedio_3 = rollmean(AlgunaVariable, k = 3, fill = NA, align = "right")) %>%
  
  ###Lagged Differences Code
  #Estados %>%
  #  group_by(Estado) %>%
  #  arrange(Date_Time) %>%
  #  #mutate(LaggedDate = lead(Date_Time), Value = as.numeric(Value)) %>%
  #  #mutate(DiffTime = abs(as.numeric(difftime(Date_Time,LaggedDate, units = "hours")))) %>%
  #  mutate(RateOfChange = (lag(Value)-Value)/(as.numeric(DiffTime))) %>%
  #  filter(Estado ==del) 
  
  ##Total R0Actual
  R0Actual <- c(R0Actual,Placeholder)
  names(R0Actual) <- Nombres
  R0Actual <- R0Actual %>% as_tibble()
  #TotalConfirmados
  TotalConfirmados <- c(TotalConfirmados,UltimaF %>% select(Obs_Hospital))
  names(TotalConfirmados) <- Nombres
  TotalConfirmados <- TotalConfirmados %>% as_tibble()
  ##TasaIncidencia10
  TasaIncidencia10 <- c(TasaIncidencia10,Tasas %>% filter(NombreOficial == Estado) %>% select(inc))
  names(TasaIncidencia10) <- Nombres
  TasaIncidencia10 <- TasaIncidencia10 %>% as_tibble()
  ##TotalMuertos
  #TotalMuertos <- c(TotalMuertos,UltimaF %>% select(UCI))
  #names(TotalMuertos) <- Nombres
  #TotalMuertos <- TotalMuertos %>% as_tibble()
  ##TasaLetalidad
  #TasaLetalidad <- c(TasaLetalidad,Placeholder)
  #names(TasaLetalidad) <- Nombres
  #TasaLetalidad <- TasaLetalidad %>% as_tibble()
  ##Recuperados
  Recuperados <- c(Recuperados,UltimaF %>% select(Obs_Alta_Mejoria_Egreso))
  names(Recuperados) <- Nombres
  Recuperados <- Recuperados %>% as_tibble()
  
  
  Tabla <- rbind(
    #R0Actual,
    TotalConfirmados,
    TasaIncidencia10,
    #TotalMuertos,
    #TasaLetalidad,
    Recuperados
  )
}

DesgloseEstados <- function(){
  Nombres <- c("Estado","Cambio de casos","Cambio de mortalidad","% Ocupacion")
  #Rolling mean?
  
  CambioCasos<- c()
  CambioMort <- c()
  PorcOcu <- c()
}




library(sf)
mexico <- st_read(dsn = 'data/Maps',
                  layer = '00ent')



datosINC <- merge(TasasData %>% mutate(Num = ifelse(Num < 10, paste0("0",Num),Num)) %>% filter(Num != "00"), mexico, by.x = 'Num', by.y = 'CVEGEO', all.y = T)



p <- ggplot(datosINC %>% select(geometry,inc)) +
  geom_sf(aes(fill = inc, geometry = geometry), color = 'black', size = 0.3)+
  #guides(fill = guide_legend(reverse = T)) +
  scale_fill_gradient(low = '#c4eeff', high = '#003459',
                      name = '') +
  #guides(fill = guide_legend(reverse = T)) +
  labs(caption = 'SINOLAVE', title = 'Incidencia de COVID en Mexico',
       subtitle = 'Cada 10,000 habitantes') +
  theme(
    plot.background = element_rect(fill = 'white'),
    panel.background = element_rect(fill = 'white'),
    axis.line = element_blank(),
    axis.title = element_blank(),
    axis.ticks = element_blank(),
    axis.text = element_blank(),
    plot.title = element_text(family = 'Times', size = 24, color = 'black',
                              hjust = 0, face = 'bold'),
    plot.subtitle = element_text(family = 'Times', size = 12, color = 'black',
                                 hjust = 0),
    plot.caption = element_text(family = 'Times', size = 14, color = 'black', hjust = 1),
    legend.direction = 'vertical',
    legend.position = 'right',
    legend.text = element_text(family = 'Times', size = 14, color = 'black'),
    legend.title = element_text(family = 'Times', size = 14, color = 'black'),
    legend.justification="center"
  )
